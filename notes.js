/**
 * Notes data access object.
 * @constructor
 * @param {Database} database
 */
function Notes(database) {
  this.database = database;
  this.createTable();
}

/**
 * SQL table creation.
 * @memberof Notes
 */
Notes.prototype.createTable = function() {
  this.database.ExecuteSql( "CREATE TABLE IF NOT EXISTS notes" + 
    "(id integer primary key autoincrement, " + 
    "ideaId integer, " + //foreign key
    "title text, " + 
    "content text)" );
};

/**
 * Call function 'callback' with results of select.
 * @memberof Notes
 * @param {Function} callback - Run on success.
 */
Notes.prototype.getAll = function(callback) {
  this.database.ExecuteSql("SELECT * FROM notes", [], callback);
};

/**
 * Add a new note to the table.
 * @memberof Notes
 * @param {Note} note - The note to add to table.
 * @param {Function} callback - Run on failure.
 */
Notes.prototype.insert = function(note, callback) {
  this.database.ExecuteSql("INSERT INTO notes (ideaId, title, content) VALUES (?,?,?)", [note.ideaId, note.title, note.content], null, callback);
};

/**
 * Remove the note with a certain id.
 * @memberof Notes
 * @param {Integer} id 
 */
Notes.prototype.removeById = function(id) {
  this.database.ExecuteSql("DELETE FROM notes WHERE id = ?", [id]);
};

