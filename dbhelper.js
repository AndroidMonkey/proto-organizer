/**
 * Database helper opens the database and returns it.
 * @returns {Database}
 */
function databaseHelper() {
  var databasePath = "/sdcard/organizerv7/system/organizerDB";
  return app.OpenDatabase(databasePath);
}
