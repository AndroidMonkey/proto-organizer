/* eslint-disable no-unused-vars */
/* global app:false */

/**
 * Represents the UI.
 * Written as a singleton to avoid multiple uses.
 * ( from singleton pattern at http://robdodson.me/javascript-design-patterns-singleton/ )
 */
function Menubar(layout) {
  "use strict";

  // do we have an existing instance? 
  if (typeof Menubar.instance === "object") { 
    return Menubar.instance; 
  }
  var uix = app.CreateUIExtras();

  this.layout = app.CreateLayout("Linear","Horizontal,FillX");
  this.layout.SetBackColor("#2a2b2c");
  this.layout.SetPadding(0.0045, 0.0045, 0.0045, 0.0045);
  this.layout.SetGravity("HCenter,VCenter");

  var title = app.CreateText("OrganizerV7 ");
  title.SetTextSize(36);
  this.layout.AddChild(title);
  
  var settings = uix.CreateFAButton("[fa-cog]", "mini");
  settings.SetButtonColors("silver");
  settings.SetOnTouch(btnSettings_OnTouch);
  this.layout.AddChild(settings);
  
  var favorites = uix.CreateFAButton("[fa-heart]", "mini");
  favorites.SetButtonColors("#b33c43");
  favorites.SetOnTouch(btnFavorites_OnTouch);
  this.layout.AddChild(favorites);

  var downloads = uix.CreateFAButton("[fa-download]", "mini");
  downloads.SetButtonColors("#3cb371");
  downloads.SetOnTouch(btnDownloads_OnTouch);
  this.layout.AddChild(downloads);
  
  layout.AddChild(this.layout);

  Menubar.instance = this;
}

function btnSettings_OnTouch() {
  "use strict";

  app.ShowPopup("settings");
}

function btnFavorites_OnTouch() {
  "use strict";

  app.ShowPopup("favorites");
}

function btnDownloads_OnTouch() {
  "use strict";

  app.ShowPopup("downloads");
}

