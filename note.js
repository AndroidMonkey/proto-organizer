/**
 * Represents a note object.
 * @param {Integer} id
 * @param {Integer} ideaId 
 * @param {String} title
 * @param {String} content
 * @constructor
 */
function Note(id, ideaId, title, content) {
  "use strict";
  
  this.id = id;
  this.ideaId = ideaId;
  this.title = title;
  this.content = content;
}

