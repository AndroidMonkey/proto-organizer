/* eslint-disable no-unused-vars */
/* global app:false, leftDrawer */

/**
 * Creates a structured UI layout for the left drawer.
 * @param {Object} dataSource
 */
function LeftDrawer(dataSource) {
  "use strict";

  /** @property {Layout} layout */
  this.layout = app.CreateLayout("Linear","FillXY");
  this.layout.SetBackColor("#ff8888");

  var title = app.CreateText("Ideas",1,0.1);
  this.layout.AddChild(title);

  /** @property {List} list */
  this.list = app.CreateList("",1,0.9,"WhiteGrad");
  this.list.SetTextColor1( "#ff555558");
  this.list.SetTextColor2( "#ff555558" );
  this.list.SetTextMargins( 0.04, 0, 0, 0 ); 
  this.layout.AddChild(this.list);
}

LeftDrawer.prototype.update = function () {
  "use strict";

}


/**
 * Creates a structured UI layout for the right drawer.
 * @param {Object} dataSource
 */
function RightDrawer(dataSource) {
  "use strict";

  /** @property {Layout} layout */
  this.layout = app.CreateLayout("Linear","FillXY");
  this.layout.SetBackColor("#8888ff");

  var title = app.CreateText("Notes",1,0.1);
  this.layout.AddChild(title);

  /** @property {List} list */
  this.list = app.CreateList("",1,0.9,"WhiteGrad");
  this.list.SetTextColor1( "#ff555558");
  this.list.SetTextColor2( "#ff555558" );
  this.list.SetTextMargins( 0.04, 0, 0, 0 ); 
  this.layout.AddChild(this.list);
}

//Called when a drawer is opened or closed.
function OnDrawer(side, state) {
  "use strict";

  if(side=="Left" && state=="Open") {
    leftDrawer.list.SetList("a,b,c");
  }
}
