/**
 * Ideas data access object.
 * @param {object} database
 * @constructor
 */
function Ideas(database) {
  "use strict";

  this.database = database;
  this.createTable();
}

/**
 * SQL table creation.
 * @memberof Ideas
 */
Ideas.prototype.createTable = function() {
  "use strict";

  this.database.ExecuteSql( "CREATE TABLE IF NOT EXISTS ideas (id integer primary key autoincrement, title text, content text, color text)" );
};

/**
 * Call function 'callback' with results of select.
 * @memberof Ideas
 * @param {function} callback - Run on success.
 */
Ideas.prototype.getAll = function(callback) {
  "use strict";

  this.database.ExecuteSql("SELECT * FROM ideas", [], callback);
};

/**
 * Add a new idea to the table.
 * @memberof Ideas
 * @param {Idea} idea - The idea to add to table.
 * @param {function} callback - Run on failure.
 */
Ideas.prototype.insert = function(idea, callback) {
  "use strict";

  this.database.ExecuteSql("INSERT INTO ideas (title, content, color) VALUES (?,?,?)", [idea.title, idea.content, idea.color], null, callback);
};

/**
 * Remove the note with a certain id.
 * @memberof Ideas
 * @param {number} id 
 */
Ideas.prototype.removeById = function(id) {
  "use strict";

  this.database.ExecuteSql("DELETE FROM notes WHERE id = ?", [id]);
};

