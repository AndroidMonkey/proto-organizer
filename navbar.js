/* eslint-disable no-unused-vars */
/* global app:false */

/**
 * Represents the UI.
 * Written as a singleton to avoid multiple uses.
 * ( from singleton pattern at http://robdodson.me/javascript-design-patterns-singleton/ )
 */
function Navbar(layout) {
  "use strict";

  // do we have an existing instance? 
  if (typeof Navbar.instance === "object") { 
    return Navbar.instance; 
  }

  this.layout = app.CreateLayout("Linear","Horizontal,FillX");
  this.layout.SetBackColor("silver");
  this.layout.SetPadding(0.0045, 0.0045, 0.0045, 0.0045);
  //this.layout.SetGravity("HCenter,Vcenter");

  var ideaNew = app.CreateButton("[fa-lightbulb-o] Create Idea ", -1, -1, "FontAwesome");
  ideaNew.SetOnTouch(btnIdeaNew_OnTouch);
  this.layout.AddChild(ideaNew);
  
  var ideasAll = app.CreateButton("[fa-list] Ideas", -1, -1, "FontAwesome");
  ideasAll.SetOnTouch(btnIdeasAll_OnTouch);
  this.layout.AddChild(ideasAll);

  var notesAll = app.CreateButton("[fa-sticky-note] Notes ", -1, -1, "FontAwesome");
  notesAll.SetOnTouch(btnNotesAll_OnTouch);
  this.layout.AddChild(notesAll);

  var noteNew = app.CreateButton("[fa-pencil] Create Note", -1, -1, "FontAwesome");
  noteNew.SetOnTouch(btnNoteNew_OnTouch);
  this.layout.AddChild(noteNew);

  layout.AddChild(this.layout);
  
  Navbar.instance = this;
}

function btnIdeaNew_OnTouch(){
createNewIdea()
}

function btnNoteNew_OnTouch(){
createNewNote()

}

function btnIdeasAll_OnTouch(){
  app.OpenDrawer( "left" );

}

function btnNotesAll_OnTouch(){

app.OpenDrawer( "right" );
}