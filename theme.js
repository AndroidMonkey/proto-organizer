function Theme (base) {
  var theme = app.CreateTheme(base);
  switch (base) {
	case "dark":
	  app.ShowPopup("creating theme");
      theme.SetButtonOptions( "custom" );
      theme.SetButtonStyle( "#353535","#161616",2,"#222222",1,1,"#00E1B6" );
      theme.SetBtnTextColor( "#bbffffff" );
      theme.SetBackColor( "#99000000" );
      theme.SetDialogBtnColor( "#ff222222" );
      theme.SetDialogBtnTxtColor( "#ffffff" );
      theme.SetTitleHeight( 42 );
	  break;
  }
  return theme;                        
}
