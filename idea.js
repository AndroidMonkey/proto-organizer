/* eslint-disable no-unused-vars */
/**
 * Represents an idea object.
 * @constructor
 * @param {number} id
 * @param {string} title
 * @param {string} content
 * @param {string} color
 */
function Idea(id, title, content, color) {
  "use strict";

  this.id = id;
  this.title = title;
  this.content = content;
  this.color = color;
}

