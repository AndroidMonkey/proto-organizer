/**
 * Proto Organizer
 * @copyright Jared Baressi
 * @license "Ask Jared"
 */
/* eslint-disable no-unused-vars */
/* global app:false, databaseHelper, LeftDrawer, RightDrawer, Idea, Ideas, Menubar, Navbar, Note, Notes, Theme */
app.LoadPlugin("UIExtras");
app.LoadScript("dbhelper.js");
app.LoadScript("drawers.js");
app.LoadScript("idea.js");
app.LoadScript("ideas.js");
app.LoadScript("menubar.js");
app.LoadScript("navbar.js");
app.LoadScript("note.js");
app.LoadScript("notes.js");
app.LoadScript("theme.js");

var leftDrawer;
var rightDrawer;

function OnStart() {
  "use strict";

  app.SetTheme(new Theme("Dark"));
  var layout = app.CreateLayout("Linear","FillXY");
  var database = databaseHelper();
  var menu = new Menubar(layout);
  var nav = new Navbar(layout);
  var ideas = new Ideas(database);
  var notes = new Notes(database);
  leftDrawer = new LeftDrawer(ideas);
  rightDrawer = new RightDrawer(notes);
  //ideas.getAll(onfetch);
  //notes.getAll(onfetch);

  app.AddLayout(layout);
  app.AddDrawer(leftDrawer.layout, "left", 1);
  app.AddDrawer(rightDrawer.layout, "right", 1);
}

function onfetch(results){
  "use strict";

  app.OpenDrawer("left");
  alert(JSON.stringify(results.rows.item(0)));
  //app.CloseDrawer("left");
  //app.OpenDrawer("right");
  //alert("");
  //app.CloseDrawer("right");
}

/*
//app.LoadScript('createNewNote.js');
app.LoadPlugin( "UIExtras" );
app.LoadPlugin( "ImageGrid" );
app.LoadScript( "setTheme.js" );
app.LoadScript('createDatabases.js')
app.LoadScript('createNewIdea.js');
app.LoadScript('createNewNote.js');

app.LoadScript('menuBar.js');
app.LoadScript('navBar.js');
app.LoadScript('noteEditor.js');
app.LoadScript('ideaEditor.js');
app.LoadScript('noteLister.js');

app.LoadScript('ideaLister.js');


app.MakeFolder( "/sdcard/organizerv7/system/" );
app.MakeFolder( "/sdcard/organizerv7/uploads/" );
app.MakeFolder( "/sdcard/organizerv7/archives/" );
var pathDB = "/sdcard/organizerv7/system/organizerDB";
db = app.OpenDatabase(pathDB);
//Load Scripts

function OnStart() {

app.EnableBackKey(false);
createDatabases()
setTheme()

lay = app.CreateLayout("linear", "Vertical,FillXY");

var menu = new menuBar()

menu.layMain = lay;
menu.loadMenu();

var nav = new navBar();
nav.layMain = lay;
nav.loadNav();


app.AddLayout(lay);

var idea = new listIdeas() ;
idea.ideaList()     
var nlist = new listNotes ();
nlist.noteList();
}
*/
